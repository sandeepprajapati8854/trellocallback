let fs = require("fs");
function problem1(boardID, callback1) {
  setTimeout(() => {
    fs.readFile("boards.json", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        let dataArray = JSON.parse(data);
        //  console.log(dataArray);
        let villianData = callback1(
          err,
          dataArray.filter((villianDetails) => {
            return villianDetails.id === boardID;
          })
        );
        return villianData;
      }
    });
  }, 2 * 1000);
}
module.exports = problem1;
